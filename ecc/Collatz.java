package ecc;

import java.util.Scanner;
public class Collatz {
	public static void main(String args[]) {
		Scanner sc = new Scanner(System.in);
		int num = sc.nextInt();

		System.out.println(highestPowerOf2InCollatzSeq(num));

		}

		public static int highestPowerOf2InCollatzSeq(int num) {
		while (!isPowerOfTwo(num)) {
		num = nextCollatzTerm(num);
		}

		return num;
		}

		public static int nextCollatzTerm(int num) {
		if (num % 2 == 1)
		return num * 3 + 1;
		else
		return num / 2;
		}

		public static boolean isPowerOfTwo(int n) {
		if (n == 0)
		return false;
		while (n != 1) {
		if (n % 2 != 0)
		return false;
		n = n / 2;
		}
		return true;
		}
		}



