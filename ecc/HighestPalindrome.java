package ecc;

import java.util.Scanner;

public class HighestPalindrome {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int num = sc.nextInt();

		System.out.println(getNextPalindrome(num + 1));

		// TODO Auto-generated method stub

	}

	private static int getHighestPalindrome(int num) {

		int rev = 0;
		while (num > 0) {
			int rem = num % 10;
			rev = rev * 10 + rem;
			num /= 10;
		}
		return rev;
	}

	public static boolean isPalindrome(int num) {
		return (num == getHighestPalindrome(num));
	}

	public static int getNextPalindrome(int num) {
		while (!isPalindrome(num)) {
			num++;
		}
		return num;
	}

}
