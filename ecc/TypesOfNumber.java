package ecc;

public class TypesOfNumber {
	public static void main(String[] args) {
        int num = 28;
        int res = sumOfProperDivisors(num);
        if (res == 0)
            System.out.println("Perfect Number");
        else if (res == 1) 
            System.out.println("Abundant Number");
        else if (res == -1)
            System.out.println("Deficient Number");
        else
            System.out.println("Please Enter Positive Number");
    }

    public static int sumOfProperDivisors(int num) {

		int res = 0;
		
		if (num < 0)

			return -2;
		
		else if (num == 0)
			
			return -3;
		
		else{

		for (int i = 1; i < num; i++) {

			if (num % i == 0) {

				res = res + i;
			}
		}
		if (res == num)

			return 0;

		else if (res > num)

			return 1;

		else if (res < num)

			return -1;
		}
		
		return res;

		// ADD YOUR CODE HERE
	}
    }



