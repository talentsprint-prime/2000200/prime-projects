package ecc;

import java.util.Scanner;

public class TwoOfThreeBooleanArgumentsAreTrue {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		boolean a = sc.nextBoolean();
		boolean b = sc.nextBoolean();
		boolean c = sc.nextBoolean();

		System.out.println(getTwoOfThreeBooleanArgumentsAreTrue(a, b, c));

	}

	private static boolean getTwoOfThreeBooleanArgumentsAreTrue(boolean a, boolean b, boolean c) {
		if (a && b && c)
			return false;
		else if ((a && b) || (b && c) || (c && a))

			return true;
		else
		return false;

	}
}
