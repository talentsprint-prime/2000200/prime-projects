package ecc;

import java.util.Scanner;

public class Aliquot {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int num = sc.nextInt();
		getSumOfFactors(num);
		// TODO Auto-generated method stub

	}

	private static void getSumOfFactors(int num) {
		int sum = 1;
		for (int i = 2; i*i <= num; i++) {
			if (num % i == 0)
				sum = sum + i+(num/i);

		}
		System.out.println(sum);
	}

}
