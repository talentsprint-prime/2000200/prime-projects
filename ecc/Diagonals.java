package ecc;
import java.util.Scanner;
public class Diagonals {

	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		int arr[][]=new int[3][3];
		for(int i=0;i<3;i++){
			for(int j=0;j<3;j++){
				arr[i][j]=sc.nextInt();
			}
			System.out.println();
			}
		for(int i=0;i<3;i++){
			for(int j=0;j<3;j++){
				System.out.println(arr[i][j]+" ");
			}
			System.out.println();
			}
		System.out.println("sum of diagonals elements is "+getSumOfDiagonals(arr));
		}
	public static int getSumOfDiagonals(int[][] arr){
		int res=0;
		for(int i=0;i<3;i++){
			for(int j=0;j<3;j++){
				if(i==j)
					res=res+arr[i][j];
			}
		}
		return res;	
	}

}
